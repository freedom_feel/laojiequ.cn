package models

import (
	"database/sql"
	"errors"
	"fmt"
	"math"

	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

var db *gorm.DB

type User struct {
	Username    string  `json:"username"`
	Password    string  `json:"password"`
	Email       string  `json:"email"`
	Mobile      string  `json:"mobile"`
	UserBalance float64 `json:"user_balance"`
	UserPhoto   string  `json:"user_photo"`
	Time
}

type Goods struct {
	GoodsName      string  `json:"goods_name"`
	GoodsPrice     float64 `json:"goods_price"`
	GoodsInventory int64   `json:"goods_inventory"`
	GoodsTypeId    int64   `json:"goods_type_id"`
	SoldNum        int64
	IsHot          int64
	Img            string
	Time
}

type Time struct {
	ID        uint `gorm:"primarykey"`
	CreatedAt string
	UpdatedAt string
	DeletedAt sql.NullTime `gorm:"index"`
}

func init() {
	dsn := "root:wm123@tcp(laojiequ.cn:3306)/WM"
	var err error
	db, err = gorm.Open(mysql.Open(dsn), &gorm.Config{})
	if err != nil {
		fmt.Println(err)
		return
	}
	db.Debug()
	fmt.Println("Database Success")
}

func UserLoginByMobile(mobile string) (User, error) {
	var u User
	res := db.Table("user").Where("mobile = ?", mobile).First(&u)
	if res.Error != nil {
		fmt.Println(res.Error)
		return User{}, errors.New("该手机不存在，请先注册！！！")
	}
	return u, nil
}

func UserLoginByUsername(username, password string) (User, error) {
	var u User
	res := db.Table("user").Where("username = ?", username).First(&u)
	if res.Error != nil {
		fmt.Println(res.Error)
		return User{}, errors.New("用户名不存在，请先注册！！！")
	}
	if u.Password != password {
		return User{}, errors.New("Password Error")

	}
	return u, nil
}

func UserInsert(mobile string, u *User) error {
	var us User
	res := db.Table("user").Where("mobile=?", mobile).First(&us)
	if res.Error == nil {
		return errors.New("改手机号已注册！！！")
	}
	res = db.Table("user").Create(u)
	if res.Error != nil {
		fmt.Println(res.Error)
		return errors.New("用户注册失败")
	}
	return nil

}

func FenYe(page int64) ([]Goods, int64) {
	var g []Goods
	//总数据
	res := db.Table("goods").Find(&g)
	count := res.RowsAffected
	//每页展示多少条数据
	size := 5
	//展示多少页
	pages := math.Ceil(float64(count / int64(size)))
	var nowPage int64
	var offset int64
	if page != 0 {
		//当前页
		nowPage = page
		//偏移量
		offset = (nowPage - 1) * int64(size)
		db.Table("goods").Offset(int(offset)).Limit(size).Find(&g)
		return g, int64(pages)
	}
	nowPage = 0
	offset = (nowPage - 1) * int64(size)
	fmt.Println(pages)
	db.Table("goods").Offset(int(offset)).Limit(size).Find(&g)
	return g, int64(pages)
}

func List(page int, name string) (int, float64, []Goods) {
	var g []Goods
	//当前页
	if page == 0 {
		page = 1
	}
	//总数据数
	var search string
	if name != "" {
		search = "%" + name + "%"
		res := db.Table("goods").Where("name like ?", search).Find(&g)
		count := res.RowsAffected
		//显示多少页
		showPage := 5
		//总页数
		pages := math.Ceil(float64(count) / float64(showPage))
		//偏移量
		offset := (page - 1) * showPage
		//数据
		//o.QueryTable("good").RelatedSel("goods_type").Filter("name__contains", name).Offset(offset).Limit(showPage).All
		db.Table("goods").Where("name like ?", search).Offset(offset).Limit(showPage).Find(&g)
		return showPage, pages, g
	}

	res := db.Table("goods").Find(&g)
	count := res.RowsAffected
	//显示多少页
	showPage := 5
	//总页数
	pages := math.Ceil(float64(count) / float64(showPage))
	//偏移量
	offset := (page - 1) * showPage
	//数据
	//o.QueryTable("good").RelatedSel("goods_type").Filter("name__contains", name).Offset(offset).Limit(showPage).All
	db.Table("goods").Offset(offset).Limit(showPage).Find(&g)
	return showPage, pages, g
}
