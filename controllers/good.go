package controllers

import (
	"fmt"
	"strconv"

	beego "github.com/beego/beego/v2/server/web"

	"laojiequ.cn/models"
)

type GoodController struct {
	beego.Controller
}

type GoodList struct {
	NowPage  int
	ShowPage int
	Pages    int
	Data     []models.Goods
	Name     string
	Fy       []FY
}

type FY struct {
	Ym   int
	Name string
}

func (c *GoodController) List() {
	//page := c.GetString("page")
	////utils.ParseToken()
	//
	//pagInt, _ := strconv.Atoi(page)
	//g, p := models.FenYe(int64(pagInt))
	//c.Data["json"] = map[string]interface{}{
	//	"message": "分页查询成功",
	//	"code":    200,
	//	"pages":   p,
	//	"data":    g,
	//}
	//c.ServeJSON()

	page := c.GetString("page")
	name := c.GetString("name")

	//logs.Info(page)
	//logs.Info(name)
	p, _ := strconv.Atoi(page)
	showPage, pages, g := models.List(p, name)

	//分页
	var f []FY
	for i := 1; i <= int(pages); i++ {
		f = append(f, FY{
			Ym:   i,
			Name: name,
		})
	}
	//上一页
	var upPage, nextPage int
	if p-1 > 0 {
		upPage = p - 1
	} else {
		upPage = 1
	}
	//下一页
	if p+1 < int(pages) {
		nextPage = p + 1
	} else {
		nextPage = int(pages)
	}
	GL := GoodList{
		NowPage:  p,
		ShowPage: showPage,
		Pages:    int(pages),
		Data:     g,
		Name:     name,
		Fy:       f,
	}
	fmt.Println(GL)
	//JsGl, _ := json.Marshal(GL)
	//fmt.Println(JsGl)
	c.Data["upPage"] = upPage
	c.Data["nextPage"] = nextPage
	c.Data["NowPage"] = GL.NowPage
	c.Data["ShowPage"] = GL.ShowPage
	c.Data["Pages"] = GL.Pages
	c.Data["Data"] = GL.Data
	c.Data["Name"] = GL.Name
	c.Data["Fy"] = GL.Fy
	c.TplName = "list.html"
}

func (c *GoodController) DeleteGood() {
	c.Data["json"] = map[string]interface{}{
		"code":    200,
		"message": "这里是删除，待开发",
	}
	c.ServeJSON()
}

func (c *GoodController) UpdateGood() {
	c.Data["json"] = map[string]interface{}{
		"code":    200,
		"message": "这里是修改，待开发",
	}
	c.ServeJSON()
}
