package utils

import (
	"fmt"
	"math/rand"
	"strconv"
)

func CreateCode() int {
	var code string
	for i := 1; i < 7; i++ {
		code += fmt.Sprintf("%d", rand.Intn(9))
	}
	sms, _ := strconv.Atoi(code)
	return sms
}
