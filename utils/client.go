package utils

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/go-redis/redis/v8"
	"strconv"
	"time"
)

var rdb *redis.Client

func init() {
	rdb = redis.NewClient(&redis.Options{
		Addr: "laojiequ.cn:6379",
		DB:   1,
	})
}

func RedisSet(key string, value interface{}) error {
	data, _ := json.Marshal(value)
	_, err := rdb.Set(context.Background(), key, data, 5*time.Minute).Result()
	if err != nil {
		return err
	}
	return nil
}

func RedisGet(key string) (string, error) {
	res, err := rdb.Get(context.Background(), key).Result()
	if err != nil {
		return "", err
	}

	return res, nil
}

func RedisIncr(key string) {
	_, err := rdb.Incr(context.Background(), key).Result()
	if err != nil {
		fmt.Println(err)
		return
	}
	return
}

func RedisTime(key, value string) error {
	_, err := rdb.Set(context.Background(), key, value, 5*time.Minute).Result()
	if err != nil {
		fmt.Println(err)
		return err
	}
	return nil
}

func RedisExist(key string) bool {
	_, err := rdb.Exists(context.Background(), key).Result()
	if err != nil {
		return false
	}
	return true
}

func GetRedisCode(key string) int {
	result, err := rdb.Get(context.Background(), key).Result()
	if err != nil {
		fmt.Println(err)
		return 0
	}
	code, _ := strconv.Atoi(result)
	return code
}
