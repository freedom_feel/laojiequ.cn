package routers

import (
	beego "github.com/beego/beego/v2/server/web"
	"laojiequ.cn/controllers"
)

func init() {
	beego.Router("/form", &controllers.UserController{}, "get:Form")
	//修改
	beego.Router("/updatePaw", &controllers.UserController{}, "get:UpdatePaw")
	//注册
	beego.Router("/Register", &controllers.UserController{}, "post:Register")
	//发送验证码
	beego.Router("/sendSms", &controllers.UserController{}, "post:SendSms")
	beego.Router("/login", &controllers.UserController{}, "post:Login")
	//注册表单
	beego.Router("/registerForm", &controllers.UserController{}, "get:RegisterForm")
	//商品展示
	beego.Router("/list", &controllers.GoodController{}, "get:List")
	//删除
	beego.Router("/delete", &controllers.GoodController{}, "get:DeleteGood")
	//修改
	beego.Router("/update", &controllers.GoodController{}, "get:UpdateGood")
}
